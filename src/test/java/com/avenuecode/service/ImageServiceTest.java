/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.service;

import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class ImageServiceTest {

    @EJB
    ProductService instance;
    @EJB
    ImageService instanceImg;

    public ImageServiceTest() {
    }

    /**
     * Test of cadastrar method, of class ImageService.
     */
    @Test
    public void testCadastrar() {
        System.out.println("cadastrar");

        File fi = new File("notebook.jpg");
        byte[] imagem = null;
        try {
            imagem = Files.readAllBytes(fi.toPath());
        } catch (IOException ex) {
            Logger.getLogger(ImageServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance = new ProductService();

        Image img = new Image();
        //img.setImage(imagem);
        img.setDescription("Teste de descricao C");

        instanceImg = new ImageService();

        Response response = instanceImg.cadastrar(img);

        assertEquals("status 200", 200, response.getStatus());
    }

    /**
     * Test of alterar method, of class ImageService.
     */
    @Test
    public void testAlterar() {
        System.out.println("alterar");
        Image img = new Image();
        instanceImg = new ImageService();

        Response output = instanceImg.alterar(img);
        assertEquals("status 200", 200, output.getStatus());
    }

    /**
     * Test of excluir method, of class ImageService.
     */
    @Test
    public void testExcluir() {
        System.out.println("excluir");

        instanceImg = new ImageService();

        Image img = new Image();
        //img.setImage(imagem);
        img.setDescription("Teste de descricao A");

        instanceImg.cadastrar(img);

        instanceImg = new ImageService();

        Response output = instanceImg.excluir(img);

        assertEquals("Should return status 200", 200, output.getStatus());
    }

}
