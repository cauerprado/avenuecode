/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.service;

import com.avenuecode.entity.Product;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.EJB;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author User
 */
public class ProductServiceTest extends JerseyTest {

    @EJB
    ProductService instance;

    public ProductServiceTest() {
    }

    @Override
    protected Application configure() {
        return new ResourceConfig(ProductService.class);
    }

    /**
     * Test of cadastrar method, of class ProductService.
     */
    @Test
    public void testCadastrar() {
        System.out.println("cadastrar");

        Product product = new Product();
        product.setName("Teste C");
        product.setDescription("Teste de descricao C");
        product.setPreco(100.0);

        instance = new ProductService();

        Response response = instance.cadastrar(product);
        assertEquals("Should return status 200", 200, response.getStatus());
    }

    /**
     * Test of listar method, of class ProductService.
     */
    @Test
    public void testListar() {
        System.out.println("listar");

        instance = new ProductService();

        Product product = new Product();
        product.setName("Teste A");
        product.setDescription("Teste de descricao A");
        product.setPreco(10.0);

        instance.cadastrar(product);

        instance = new ProductService();

        List<Product> productList = new ArrayList<>();
        productList.add(product);

        List<Product> products = instance.listar();
        Assert.assertTrue(productList.contains(product));
        //assertThat(productList, is(products));
    }

    /**
     * Test of alterar method, of class ProductService.
     */
    @Test
    public void testAlterar() {
        System.out.println("alterar");
        Product product = new Product();
        instance = new ProductService();

        /*Response output = target("/product")
                .request()
                .put(Entity.entity(product, MediaType.APPLICATION_JSON));*/
        Response output = instance.alterar(product);
        assertEquals("status 200", 200, output.getStatus());
    }

    /**
     * Test of excluir method, of class ProductService.
     */
    @Test
    public void testExcluir() {
        System.out.println("Excluir");

        instance = new ProductService();

        Product product = new Product();
        product.setName("Teste A");
        product.setDescription("Teste de descricao A");
        product.setPreco(10.0);

        instance.cadastrar(product);

        instance = new ProductService();

        Response output = instance.excluir(product);

        assertEquals("Should return status 200", 200, output.getStatus());
    }

}
