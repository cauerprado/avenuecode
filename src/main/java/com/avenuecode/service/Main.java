/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.service;

import com.avenuecode.client.ImageClient;
import com.avenuecode.client.ProductClient;
import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;
import com.avenuecode.entitymanager.JpaEntityManager;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

/**
 *
 * @author User
 */
public class Main {

    public static void main(String[] args) {
        try {
            Response response;
            ProductClient client = new ProductClient();
            ImageClient iClient = new ImageClient();

            Product p = new Product();

            System.out.println("Cadastro do Produto ");

            p.setName("Teste 1");
            p.setDescription("Teste 1 preço 10,50");
            p.setPreco(10.50);

            response = client.cadastrar(p);

            System.out.println("Cadastro: " + response.getStatus() + "");

            System.out.println("Cadastro do SubProduto ");

            Product p1 = new Product();

            //Ler imagem do Produto em transformar em array de bytes
            File fi = new File("notebook.jpg");
            byte[] fileContent = Files.readAllBytes(fi.toPath());

            p1.setName("Teste Sub produto");
            p1.setDescription("Sub produto do produto 1");
            p1.setPreco(10.50);
            p1.setParent(p);

            System.out.println("Cadastro Sub Produto : " + response.getStatus() + "");

            System.out.println("Cadastro da Imagem do produto ");
            Image img = new Image();
            img.setDescription("imagem do produto 1");
            //img.setImage(fileContent);
            img.setProduct(p);
            List<Image> images = new ArrayList<>();
            p.setImages(images);

            client = new ProductClient();
            response = client.cadastrar(p);

            response = iClient.cadastrar(img);

            //System.out.println("Cadastro de imagem :" + response.getStatus());
            //Ler imagem do Produto em transformar em array de bytes
            fi = new File("tela.jpg");
            fileContent = Files.readAllBytes(fi.toPath());

            Product subproduto = new Product();

            subproduto.setDescription("Sub produto do produto já cadastrado");
            subproduto.setName("SubProduto A");
            subproduto.setParent(p1);

            System.out.println("Altera Produto ja criado e insere subProduto");
            client = new ProductClient();
            Set<Product> subprodutos = new HashSet<>();
            subprodutos.add(subproduto);
            p1.setProducts(subprodutos);
            client.alterar(p1);

            subprodutos.clear();

            client = new ProductClient();
            client.cadastrar(subproduto);

            System.out.println("Excluir Imagem e produto");
            client = new ProductClient();
            iClient = new ImageClient();
            response = iClient.exclui("1");
            System.out.println("exclusao:" + response.getStatus());
            response = client.exclui("1");
            System.out.println("exclusao:" + response.getStatus());

        } catch (Exception e) {

            e.printStackTrace();

        }

        }

    }
