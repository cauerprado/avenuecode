package com.avenuecode.service;

import com.avenuecode.entity.Image;
import com.avenuecode.entity.Product;
import com.avenuecode.entitymanager.JpaEntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Caue Prado Classe responsavel por Create, update and delete products
 *
 * cadastrar listar - lista todos os produtos alterar excluir buscar - produto
 * especifico sem relacionamentos buscarProduto - same as buscar
 * buscarProdutosImagens - produtos images e subprodutos buscaProdutosImagens -
 * produtos e imagens buscarImagensProduto imagens do produto buscarSubProdutos
 * - subprodutos de um produto
 */
@Path("/products")
@Stateless
public class ProductService {

    private JpaEntityManager JPAEM = new JpaEntityManager();
    private EntityManager objEM = JPAEM.getEntityManager();

    @POST
    @Path("/cadastrar")
    @Consumes("application/json")
    public Response cadastrar(Product product) {
        try {
            objEM.getTransaction().begin();
            objEM.persist(product);
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("cadastro realizado.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @PUT
    @Path("/cadastrarimagem/{id}")
    @Consumes("application/json")
    public Response cadastrarImagem(Image image, @PathParam("id") Integer id) {
        try {

            Product produto = objEM.getReference(Product.class, id);

            List<Image> images = new ArrayList<>();
            produto.setImages(images);

            objEM.getTransaction().begin();
            objEM.persist(produto);
            objEM.getTransaction().commit();
            objEM.close();

            image.setProduct(objEM.getReference(Product.class, id));

            objEM.getTransaction().begin();
            objEM.persist(image);
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("imagem inserida.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //Get all products excluding relationships
    @GET
    @Path("/listar")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Product> listar() {
        try {
            String query = "Select p from Product p";
            List<Product> produtos = objEM.createQuery(query, Product.class).getResultList();
            objEM.close();
            return produtos;
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @PUT
    @Path("/alterar")
    @Consumes("application/json")
    public Response alterar(Product product) {
        try {
            objEM.getTransaction().begin();
            objEM.merge(product);
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("produto alterado.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @DELETE
    @Path("/excluir")
    public Response excluir(Product product) {
        try {
            objEM.getTransaction().begin();
            objEM.remove(objEM.getReference(Product.class, product.getProductId()));
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("cadastro excluído.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @DELETE
    @Path("/exclui/{id}")
    public Response exclui(@PathParam("id") Integer id) {
        //try {
            objEM.getTransaction().begin();
            objEM.remove(objEM.getReference(Product.class, id));
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("cadastro excluído.").build();
        //} catch (Exception e) {
        //    throw new WebApplicationException(500);
        //}
    }

    @GET
    @Path("/buscar/{id}")
    @Produces("application/json")
    public Product buscar(@PathParam("id") Integer id) {
        try {
            Product product = objEM.find(Product.class, id);
            objEM.close();
            return product;
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //5 - Get a product excluding relationship by specific identity 
    @GET
    @Path("/todosprodutos/{id}")
    @Produces("application/json")
    public List<Product> buscarProduto(@PathParam("id") Integer id) {
        try {
            Query consulta = objEM
                    .createQuery(" Select p from Product p where p.productId = :productId ");
            consulta.setParameter("productId", id);
            List<Product> produtos = consulta.getResultList();

            objEM.close();

            return produtos;

        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //6) Same as 4 using specific product identity
    @GET
    @Path("/buscatodosprodutos/{id}")
    @Produces("application/json")
    public List<Product> buscarProdutosImagens(@PathParam("id") Integer id) {
        try {
            Query consulta = objEM
                    .createQuery(" Select p,i from Product p  "
                            + "   JOIN Image i where i.productId = p.productId"
                            + "   JOIN Product pc where pc.parentId = p.productId"
                            + " where p.productId = :productId ");
            consulta.setParameter("productId", id);
            List<Product> produtos = consulta.getResultList();

            objEM.close();

            return produtos;

        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //4) Get all products including specified relationships (child product and/or images)  
    @GET
    @Produces("application/json")
    public List<Product> buscaProdutosImagens() {
        try {
            Query consulta = objEM
                    .createQuery(" Select p,i from Product p  "
                            + "   JOIN Image i where i.productId = p.productId"
                            + "   JOIN Product pc where pc.parentId = p.productId");

            List<Product> produtos = consulta.getResultList();

            objEM.close();

            return produtos;

        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //8) Get set of images for specific product
    @GET
    @Path("/buscaimagensprodutos/{id}")
    @Produces("application/json")
    public List<Image> buscarImagensProduto(@PathParam("id") Integer id) {
        try {
            Query consulta = objEM
                    .createQuery(" Select i from Product p  "
                            + "   JOIN Image i where i.productId = p.productId"
                            + " where p.productId = :productId ");
            consulta.setParameter("productId", id);
            List<Image> images = consulta.getResultList();

            objEM.close();

            return images;

        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    //7) Get set of child products for specific product 
    @GET
    @Path("/buscasubprodutos/{id}")
    @Produces("application/json")
    public List<Product> buscarSubProdutos(@PathParam("id") Integer id) {
        try {
            Query consulta = objEM
                    .createQuery(" Select pc from Product p  "
                            + "   JOIN Product pc where pc.parentId = p.productId"
                            + " where p.productId = :productId ");
            consulta.setParameter("productId", id);
            List<Product> produtos = consulta.getResultList();

            objEM.close();

            return produtos;

        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }
}
