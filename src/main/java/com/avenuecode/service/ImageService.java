package com.avenuecode.service;

import com.avenuecode.entity.Image;
import com.avenuecode.entitymanager.JpaEntityManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author User
 */
@Path("/image")
@Stateless
public class ImageService {

    private JpaEntityManager JPAEM = new JpaEntityManager();
    private EntityManager objEM = JPAEM.getEntityManager();

    @POST
    @Path("/cadastrar")
    @Consumes("application/json")
    public Response cadastrar(Image image) {
      //   try {
        objEM.getTransaction().begin();
        objEM.persist(image);
        objEM.getTransaction().commit();
        objEM.close();
        return Response.status(200).entity("cadastro realizado.").build();
      //   } catch (Exception e) {
      //        throw new WebApplicationException(500);
      //  }
    }

    /**
     *
     * @return
     */
    @GET
    @Path("/listar")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Image> listar() {
        try {
            String query = "Select p from Image p";
            List<Image> images = objEM.createQuery(query, Image.class).getResultList();
            objEM.close();
            return images;
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @PUT
    @Path("/alterar")
    @Consumes("application/json")
    public Response alterar(Image image) {
        try {
            objEM.getTransaction().begin();
            objEM.merge(image);
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("imagem alterada.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }

    @DELETE
    @Path("/excluir/{id}")
    public Response excluir(Image image) {
        //try {

            objEM.getTransaction().begin();
            objEM.remove(objEM.getReference(Image.class, image.getId()));
            objEM.getTransaction().commit();
            objEM.close();

            return Response.status(200).entity("cadastro excluído.").build();
       // } catch (Exception e) {
       //     throw new WebApplicationException(500);
       // }
    }

    @DELETE
    @Path("/exclui/{id}")
    public Response exclui(@PathParam("id") Integer id) {
        try {
            objEM.getTransaction().begin();
            objEM.remove(objEM.getReference(Image.class, id));
            objEM.getTransaction().commit();
            objEM.close();
            return Response.status(200).entity("cadastro excluído.").build();
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }
    
    @GET
    @Path("/buscar/{id}")
    @Produces("application/json")
    public Image buscar(@PathParam("id") Integer id) {
        try {
            Image image = objEM.find(Image.class, id);
            objEM.close();
            return image;
        } catch (Exception e) {
            throw new WebApplicationException(500);
        }
    }
}
