/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.entitymanager;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author User
 */
public class JpaEntityManager {

    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("avenueCodePU");
    private EntityManager em = factory.createEntityManager();

    public EntityManager getEntityManager() {
        return em;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return factory;
    }

}
