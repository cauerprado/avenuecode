/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author User
 */
@Entity
@XmlRootElement(name = "product")
public class Product implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer productId;
    
    private String name;
    
    private double preco;
    
    private String description;
    
    @OneToMany(mappedBy = "product")
    private List<Image> images = new ArrayList<Image>();
    
    //child
    @OneToMany(mappedBy = "parent" , cascade={CascadeType.ALL})
    private Set<Product> products  = new HashSet<Product>() ;
    //parent
    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parentId", insertable = false, updatable = false)
    private Product parent;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
       
    public Product() {}
    

    public long getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
    
    public void adiciona(Image image){
        this.images.add(image);
        image.setProduct(this);
    }
    
    @XmlTransient
    public List<Image> getImages() {
        List<Image> listaImagens = Collections.unmodifiableList(this.images);  
        return listaImagens;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @XmlTransient
    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Product getProduto() {
        return parent;
    }

    public void setProduto(Product parent) {
        this.parent = parent;
    }

    public Product getParent() {
        return parent;
    }

    public void setParent(Product parent) {
        this.parent = parent;
    }
    
}
