/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.avenuecode.entity;

import java.io.Serializable;
import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import static javax.persistence.CascadeType.REMOVE;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */

@Entity
@XmlRootElement(name = "image")
public class Image implements Serializable {
        
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Integer id;
   
    private byte[] image;
    
    private String description;
    
    @ManyToOne(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    //@JoinColumn(name = "productId")
    private Product product;
    
    public Image(){}

    public Product getProduct() {
        return product;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
